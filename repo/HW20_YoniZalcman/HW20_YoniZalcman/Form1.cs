﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HW20_YoniZalcman
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Thread myThread = new Thread(func);
            myThread.Start();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void func()
        {
            
        }

        private string func2()
        {
            string result = "";
            string msg = "";
            Random rnd = new Random();
            int num = rnd.Next(1, 13);
            int shape = rnd.Next(1, 5);
            switch (num)
            {
                case 1:
                    result += "ace_of_";
                    break;
                case 11:
                    result += "jack_of_";
                    break;
                case 12:
                    result += "queen_of_";
                    break;
                case 13:
                    result += "king_of_";
                    break;
                default:
                    result += num.ToString() + "_of_";
                    break;
            }
            if (num > 9)
            {
                msg = num.ToString();
            }
            else
            {
                msg = "0" + num.ToString();
            }
            switch (shape)
            {
                case 1:
                    result += "clubs.png";
                    msg += "C";
                    break;
                case 2:
                    result += "diamonds.png";
                    msg += "D";
                    break;
                case 3:
                    result += "hearts.png";
                    msg += "H";
                    break;
                case 4:
                    result += "spades.png";
                    msg += "S";
                    break;
            }
            return (result + msg);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            string str=func2();
            string result=str.Substring(0,-4);
            pictureBox1.Image = HW20_YoniZalcman.Properties.Resources.result;
        }
    }
}
